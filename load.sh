#!/bin/bash

if [ $# -lt 1 ]
then
	echo "E: Illegal arguments"
	echo "N: usage: load <bin-file>"
else
	cp $1 instmem.dat
	#bash vasm_format.sh instmem.dat
fi
