#include <iostream>
#include <string>
#include <unistd.h>
#include "Vtop.h"
#include "verilated.h"

#define module Vtop

module *m;

typedef uint8_t u8;
typedef bool u;

using std::cout;
using std::flush;
using std::endl;
using std::string;
using std::stoi;

string hex_convert(int hex)
{
	switch (hex)
	{
		case 0x40:
		{
			return "0";
		}
		
		case 0x79:
		{
			return "1";
		}
		
		case 0x24:
		{
			return "2";
		}
		
		case 0x30:
		{
			return "3";
		}
		
		case 0x19:
		{
			return "4";
		}
		
		case 0x12:
		{
			return "5";
		}
		
		case 0x02:
		{
			return "6";
		}
		
		case 0x78:
		{
			return "7";
		}
		
		case 0x00:
		{
			return "8";
		}
		
		case 0x10:
		{
			return "9";
		}
		
		case 0x08:
		{
			return "A";
		}
		
		case 0x03:
		{
			return "B";
		}
		
		case 0x46:
		{
			return "C";
		}
		
		case 0x21:
		{
			return "D";
		}
		
		case 0x06:
		{
			return "E";
		}
		
		case 0x0E:
		{
			return "F";
		}
	}
	
	printf("invalid hex thing bro\n");
	exit(1);
}

int main(int argc, char **argv)
{
	if (argc < 3)
	{
		printf("E: not enough args\nN: usage: %s [SW value] [expected value]\n", argv[0]);
		exit(-1);
	}
	
	// Create an instance of our module
	m = new module;
	
	m->KEY = 0b1111;
	
	m->SW = stoi(argv[1]) & 0x3FFFF;
	
	m->eval();
	
	int cycles = 0;
	
	string actual_value = "00000000";
	
	bool finish = false;
	
	while (!(finish = Verilated::gotFinish()) && (actual_value == "00000000" && cycles < 0x1000))
	{
		m->CLOCK_50 = 1;
		m->eval();
		
		m->CLOCK_50 = 0;
		m->eval();
		
		actual_value = hex_convert(m->HEX7) + hex_convert(m->HEX6) + hex_convert(m->HEX5) + hex_convert(m->HEX4) + hex_convert(m->HEX3) + hex_convert(m->HEX2) + hex_convert(m->HEX1) + hex_convert(m->HEX0);
		
		cout << flush;
		
		cycles += 1;
	}
	
	if (finish == true)
	{
		printf("\n`$finish' detected, quitting...\n");
		return -1;
	}
	
	string expected_value = argv[2];
	
	printf("\nExpected: 0x%s\nActual:   0x%s\n", expected_value.c_str(), actual_value.c_str());
	
	if (expected_value != actual_value)
	{
		printf("\nFAILED\n");
		
		return -1;
	}
	
	printf("\nPASSED\nFinished in %d cycles.\n", cycles);
	
	exit(EXIT_SUCCESS);
}