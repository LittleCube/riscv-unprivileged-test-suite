# RISC-V Test Suite

A somewhat thorough test suite for SystemVerilog RISC-V implementations.

## Building

1. Copy your top.sv and other SystemVerilog files to the root directory, along with any mem files (e.g. instmem.dat).

2. Run `make`. This will automatically compile your SystemVerilog code and run the test script.

The script will stop if a test fails, and prints a small description of the failed test. The source code for each program is provided in the repo and all programs are short and concise (comments WIP).
