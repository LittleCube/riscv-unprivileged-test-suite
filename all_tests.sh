#!/bin/bash

readarray -t arr < test_vecs.txt
readarray -t descs < descs.txt

for i in $(seq 0 $((${#arr[@]} - 1)))
do
	
	echo ${arr[i]} | head -n1 | awk '{print $1;}'
	echo ""
	echo "description: ${descs[i]}"
	bash test.sh ${arr[i]} || exit
	echo ""
	echo ""
	
done
