filetext=$(cat "$1")

regex1_n="(..)(..)(..)(..)(.)"
regexn="(..)(..)(..)(..)"

tmptext=""

while [[ $filetext =~ $regex1_n || $filetext =~ $regexn ]];
do
	
	tmptext+="${BASH_REMATCH[4]}"
	tmptext+="\n"
	tmptext+="${BASH_REMATCH[3]}"
	tmptext+="\n"
	tmptext+="${BASH_REMATCH[2]}"
	tmptext+="\n"
	tmptext+="${BASH_REMATCH[1]}"
	tmptext+="\n"
	
	if [[ ${BASH_REMATCH[5]} = "" ]]
	then
		
		break;
		
	else
		
		filetext=${filetext#*"${BASH_REMATCH[5]}"}
		
	fi
	
done

printf "$tmptext" > "$1"
