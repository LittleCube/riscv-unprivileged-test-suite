SRCS		:= $(notdir $(wildcard tests/src/*.asm))
BINS		:= $(foreach f, $(SRCS), tests/bin/$(f:.asm=.bin))

.PHONY: all build clean

all: build
	@echo "running tests, all tests are outputted as \`instmem.dat'"
	@echo ""
	@bash all_tests.sh

build: Vtop

Vtop: *v cpp/emutest.cpp
	@echo "building design..."
	(verilator --exe --build cpp/emutest.cpp -sv -cc top.sv -j $$(nproc) 1>/dev/null && cp obj_dir/Vtop ./) || (verilator -sv -cc top.sv && g++ -Iobj_dir -I/usr/share/verilator/include cpp/emutest.cpp /usr/share/verilator/include/verilated.cpp obj_dir/Vtop.cpp obj_dir/Vtop__Syms.cpp -o Vtop)
	@echo "finished build"

bins: $(BINS)

tests/bin/%.bin: tests/src/%.asm | tests/bin
	java -jar rars.jar dump .text HexText $@ a $<

tests/bin:
	mkdir tests/bin

clean:
	rm -f Vtop
	rm -rf tests/bin
	rm -f instmem.dat
	rm -rf obj_dir
