lui and csrrw — initial test, lui doesn't need to work completely, but csrrw out does.
addi and csrrw — hardest test to pass, just try to implement addi, your csrrw out should already work.
csrrw IN 0000CAFE — make sure the cpu can take input from the switches and write them to a register
csrrw IN 0003FFFF — make sure you use all 18 bits of the switch value
csrrw rs1 — make sure you push rs1_dat to the hex displays, not rd_dat!
add — basic R—type instructions should work, with correct aluop for each one
sub — basic R—type instructions should work, with correct aluop for each one
and — basic R—type instructions should work, with correct aluop for each one
or — basic R—type instructions should work, with correct aluop for each one
xor — basic R—type instructions should work, with correct aluop for each one
sll — basic R—type instructions should work, with correct aluop for each one
sra — basic R—type instructions should work, with correct aluop for each one
srl — basic R—type instructions should work, with correct aluop for each one
slt — basic R—type instructions should work, with correct aluop for each one
slt false — basic R—type instructions should work, with correct aluop for each one
sltu — basic R—type instructions should work, with correct aluop for each one
sltu_false — basic R—type instructions should work, with correct aluop for each one
lui — cpu should be able to write all upper 20 bits of the instruction to a register
andi — basic I—type instructions should work, with correct 12—bit immediate sign-extended and correct aluop for each instruction
ori — basic I—type instructions should work, with correct 12—bit immediate sign-extended and correct aluop for each instruction
xori — basic I—type instructions should work, with correct 12—bit immediate sign-extended and correct aluop for each instruction
slli — basic I—type instructions should work, with correct 12—bit immediate sign-extended and correct aluop for each instruction
srai — basic I—type instructions should work, with correct 12—bit immediate sign-extended and correct aluop for each instruction
srli — basic I—type instructions should work, with correct 12—bit immediate sign-extended and correct aluop for each instruction
mul neg x neg — make sure you've used the right aluop, there are three different settings for different multiply instructions
mul pos x neg — make sure you've used the right aluop, there are three different settings for different multiply instructions
mulh neg x neg — make sure you've used the right aluop, there are three different settings for different multiply instructions
mulh pos x neg — make sure you've used the right aluop, there are three different settings for different multiply instructions
mulhu — make sure you've used the right aluop, there are three different settings for different multiply instructions
beq true
beq false
bge equal true
bge true
bge false
bgeu true
bgeu false
blt true
blt false
bltu true
bltu false
stall
stall false
branch address accuracy
consecutive branches
adjacent branches, take first
adjacent branches, take second
jal
jal proper address
jal linking
jal stall
jalr
jalr proper address
jalr linking
jalr stall
jalr use reg not pc